package cn.dgut.rep;

import cn.dgut.pojo.Access;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccessRep extends JpaRepository<Access,Long> {
}
