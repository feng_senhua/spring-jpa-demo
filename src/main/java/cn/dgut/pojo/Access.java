package cn.dgut.pojo;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

/**
 * 权限实体类
 */
@Data
@Entity
public class Access {
    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String accessName;
        @JoinTable(name = "user_role",joinColumns = @JoinColumn(name="user_id",referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id",referencedColumnName = "id"))
    @ManyToMany(targetEntity = Role.class)
    private List<Role> roles;
}
