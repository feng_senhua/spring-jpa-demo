package cn.dgut.pojo;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

/**
 * 角色实体
 */
@Data
@Entity
public class Role {
    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String role_name;
    @OneToMany(targetEntity = User.class,mappedBy = "role")
    private List<User> users;
    @ManyToMany(targetEntity = Access.class,mappedBy = "roles")
    private List<Access> accesses;

















//    @JoinTable(name="role_access",
//            joinColumns={@JoinColumn(name="role_id",referencedColumnName="id")},
//            inverseJoinColumns={@JoinColumn(name="access_id",referencedColumnName="id")}
//    )
//    @ManyToMany(targetEntity = Access.class,mappedBy = "roles")
//    private List<Access> accesses;

}
