package cn.dgut.pojo;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

/**
 * 用户实体
 */
@Entity
@Data
public class User {
    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String user_name;
    @ManyToOne(targetEntity = Role.class)
    @JoinColumn(name = "r_id")
    private Role role;
}
