package cn.dgut;

import cn.dgut.pojo.Access;
import cn.dgut.pojo.Role;
import cn.dgut.pojo.User;
import cn.dgut.rep.AccessRep;
import cn.dgut.rep.RoleRep;
import cn.dgut.rep.UserRep;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringJpaDemoApplicationTests {

    @Autowired
    private AccessRep accessRepository;
    @Autowired
    private RoleRep roleRepository;
    @Autowired
    private UserRep userRepository;
    @Test
    public void test() {
        Role role=new Role();
        role.setRole_name("Admin");

        User user=new User();
        User user1=new User();

        user.setUser_name("zhangsan");
        user1.setUser_name("lisi");

        user.setRole(role);
        user1.setRole(role);

        Access access=new Access();
        Access access1=new Access();
        access.setAccessName("login access");
        access1.setAccessName("log watch access");

        List<User> users=new ArrayList<>();
        users.add(user);
        users.add(user1);
        role.setUsers(users);

        List<Access> accesses=new ArrayList<>();
        accesses.add(access);
        accesses.add(access1);
        role.setAccesses(accesses);

        List<Role> roles=new ArrayList<>();
        roles.add(role);
        access.setRoles(roles);

        roleRepository.save(role);
        userRepository.save(user);
        userRepository.save(user1);
        accessRepository.save(access);
        accessRepository.save(access1);
    }

}
